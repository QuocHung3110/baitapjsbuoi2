// Bài 1: bài tập tính số ngày lương
/* Đầu vào:
- lương 1 ngày: 100.000
- số ngày làm: 30 ngày
   Các bước tính toán
- B1: Tạo 2 biến luong1ngay va songaylam cho số tiền lương 1 ngày và ngày làm.
- B2: Tạo biến tongluong cho tổng số lương của nhân viên
- B3: Gán giá trị cho luong1ngay và songaylam
- B4: Sử dụng công thức: tongluong = luong1ngay * songaylam
- B5: in kết quả ra console.
   Đầu ra:
- Tổng lương của nhân viên
 */
function tongluong() {
  var luong1ngay = document.getElementById("luong1ngay").value * 1;
  var songaylam = document.getElementById("songaylam").value * 1;
  var tongluong = luong1ngay * songaylam;
  document.getElementById("tongluong").innerHTML =
    `Tổng Lương của bạn là: ` + tongluong.toLocaleString() + ` VNĐ`;
}
//  Bài 2: Tính giá trị trung bình
/* Đầu vào:
- 5 số thực gồm : 5, 10, 15, 20, 25.
   Các bước tính toán
- B1: Tạo 5 biến cho 5 số thực
- B2: Tạo biến giatritrungbinh cho tổng 5 số thực trên
- B3: Gán giá trị cho 5 số thực trên
- B4: Sử dụng công thức: giatritrungbinh= (tổng 5 số thực)/5 
- B5: In kết quả ra console.
   Đầu ra:
- Giá trị trung bình của 5 số thực
 */
function giatritrungbinh() {
  var num1 = +document.getElementById("number1").value;
  var num2 = +document.getElementById("number2").value;
  var num3 = +document.getElementById("number3").value;
  var num4 = +document.getElementById("number4").value;
  var num5 = +document.getElementById("number5").value;
  var result = (num1 + num2 + num3 + num4 + num5) / 5;
  document.getElementById(
    "giatritrungbinh"
  ).innerHTML = `Giá trị trung bình: ${result.toLocaleString()}`;
}
// Bài 3: Quy Đổi Tiền
/* Đầu vào:
- Giá trị của VNĐ
- Số USD muốn quy đổi
   Các bước tính toán:
- B1: Tạo 2 biến giaVND và soluongUSD cho VNĐ và USD
- B2: Tạo biến giaquydoi cho 'Giá trị quy đổi'
- B3: Gán giá trị cho giaVND và soluongUSD
- B4: Sử dụng công thức: giaquydoi = giaVND * soluongUSD
- B5: in kết quả ra console
   Đầu ra:
- Giá trị quy đổi.
 */
function quydoitien() {
  var giaVND = 23500;
  var soluongUSD = +document.getElementById("soluongUSD").value;
  var giaquydoi = giaVND * soluongUSD;
  document.getElementById(
    "giaquydoi"
  ).innerHTML = ` Tương Đương: ${giaquydoi.toLocaleString()}`;
}
// Bài 4: Tính diệ tích, chu vi hình chữ nhật
/* Đầu vào:
- Chiều Dài: 20cm
- chiều rộng: 5cm
   Các bước tính toán:
- B1: Tạo biến chieudai và chieurong cho chiều dài, chiều rộng của hình chữ nhật
- B2: Tạo biến chuvi và dientich cho chu vi, diện tích của hình chữ nhật
- B3: Gán giá trị cho 2 biến chieudai và chieurong
- B4: Sử dụng công thức chuvi = (chieudai + chieurong) * 2 và dientich = chieudai * chieurong 
- B5: In kết quả ra Console
   Đầu ra:
- Chu vi và diện tích của hình chữ nhật 
 */
function chuvi() {
  var chieudai = +document.getElementById("chieudai").value;
  var chieurong = +document.getElementById("chieurong").value;
  var chuvi = (chieudai + chieurong) * 2;
  var dientich = chieudai * chieurong;
  document.getElementById(
    "chuvi"
  ).innerHTML = `Chu Vi:  ${chuvi.toLocaleString()}`;
  document.getElementById(
    "dientich"
  ).innerHTML = `Diện Tích:  ${dientich.toLocaleString()}`;
}
// Bài 5: Tính Tổng 2 Ký số
/* Đầu vào:
- Số có 2 chữ số
   Các bước tính toán:
- B1: tạo biến number0 và gán biến cho số
- B2: tạo biến tong2kyso cho tổng 2 ký số
- B3: tách số hàng chục theo công thức hangchuc = Math.floor (n / 10), hangdonvi = Math.floor (n % 10)
- B4: sử dụng công thức tong2kyso = hangchuc + hangdonvi
- B5: in kết quả ra console.
   Đầu ra:
- Tổng 2 ký số
*/
function tong2kyso() {
  var numbervalue = +document.getElementById("numbervalue").value;
  var hangchuc = Math.floor(numbervalue / 10);
  var hangdonvi = Math.floor(numbervalue % 10);
  var tong2kyso = hangchuc + hangdonvi;
  document.getElementById(
    "tong2kyso"
  ).innerHTML = `Tổng hai ký số: ${tong2kyso.toLocaleString()}`;
}
